package com.buggerspace.notificationservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;

import com.buggerspace.notificationservice.domain.Notification;
import com.buggerspace.notificationservice.domain.UserNotification;
import com.buggerspace.notificationservice.repository.UserNotificationRepository;
import com.buggerspace.notificationservice.service.UserNotificationService;

@SpringBootTest
public class TestNotification {

	//UserNotification userNotification = null;
	
	@Autowired
	UserNotificationService userNotificationService;
	
	//@Autowired
	//UserNotificationRepository userNotificationRepository;
	
//	@BeforeEach
//	public void setUp() {
//		 userNotificationService = new UserNotificationService();
//	}
	
	@Test
	@Order(1)
	void addUserNotification() {
	
		//Given
		userNotificationService.deleteUserNotificationByEmail("test@gmail.com");
		UserNotification userNotification=userNotificationService.create("1","test@gmail.com", true ,true);
		
		//When
		UserNotification userNotificationFind = userNotificationService.findByUserId(userNotification.getUserId());
		
		//Then
		assertEquals("test@gmail.com",userNotificationFind.getEmail());
	}
	
	@Test
	@Order(2)
	public void ensureUserEmailUniqueness() {
	    //Given
		userNotificationService.deleteUserNotificationByEmail("testdeneme@gmail.com"); //clean up first
		userNotificationService.create("11","testdeneme@gmail.com", true ,true); //first time

	    //When
	    RuntimeException exception = assertThrows(RuntimeException.class, () ->
	    userNotificationService.create("12","testdeneme@gmail.com", true ,true) //second time
	    );

	    //Then
	    assertEquals("There is already a user email as: testdeneme@gmail.com", exception.getMessage());
	  }
	
	@Test
	@Order(3)
	void addNotification() {
		
		//Given
		Notification notification = new Notification("10" , "Masa" ,LocalDateTime.now());
		//userNotificationService
		
		//When
		UserNotification userNotification = userNotificationService.findByUserId("1");
		userNotification.setNotifications(notification);
		
		userNotification.getNotifications().contains(notification);
		//Then
		assertEquals(true,userNotification.getNotifications().contains(notification));
	}
	
	@Test
	@Order(4)
	void findDeletedUserException() {
		//Given
		userNotificationService.deleteUserNotificationByEmail("test@gmail.com");
		
		//When
	    RuntimeException exception = assertThrows(RuntimeException.class, () ->
	    userNotificationService.findByUserId("1") 
	    );

	    //Then
	    assertEquals("User not found with id: 1", exception.getMessage());
	}
	
	
	
}
