package com.buggerspace.notificationservice.repository;


import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.data.couchbase.repository.ScanConsistency;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.buggerspace.notificationservice.domain.UserNotification;
import com.couchbase.client.java.query.QueryScanConsistency;

@Repository
public interface UserNotificationRepository extends CouchbaseRepository<UserNotification,String> {

	Object deleteByEmail(String email);

	@ScanConsistency(query = QueryScanConsistency.REQUEST_PLUS)
	boolean existsByEmail(String email);

}
