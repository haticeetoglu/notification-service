package com.buggerspace.notificationservice.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.buggerspace.notificationservice.domain.Notification;
import com.buggerspace.notificationservice.domain.UserNotification;
import com.buggerspace.notificationservice.repository.UserNotificationRepository;

@Service
public class UserNotificationService {

	@Autowired
	private UserNotificationRepository userNotificationRepository;

	public UserNotification create(UserNotification userNotification) {

//		if(userNotificationRepository.existsByEmail(userNotification.getEmail())) {
//		      throw new RuntimeException("There is already a user email as: "+userNotification.getEmail());
//		    }
		
		return userNotificationRepository.save(userNotification);
	}

	public UserNotification create(String userId, String email, boolean webNotificationsIsActive, boolean mobileNotificationsIsActive) {
		
		if(userNotificationRepository.existsByEmail(email)) {
		      throw new RuntimeException("There is already a user email as: "+email);
		    }
		
			UserNotification userNotification = new UserNotification(userId, email, webNotificationsIsActive, mobileNotificationsIsActive);
		return userNotificationRepository.save(userNotification);
	}

	public void deleteUserNotificationByEmail(String email) {
    	userNotificationRepository.deleteByEmail(email);
    }

	
	public UserNotification findByUserId(String id) {
		
		return userNotificationRepository.findById(id)
				.orElseThrow(() -> new RuntimeException(String.format("User not found with id: %s", id)));
	}

	public void addNotificationToUserNotification(String userId, String productId, String productName, LocalDateTime notificationDate) {

		UserNotification userNotification = userNotificationRepository.findById(userId)
				.orElseThrow(RuntimeException::new);
		
		List<Notification> notifications = userNotification.getNotifications();
		Notification notification = new Notification(productId, productName, notificationDate);
		notifications.add(notification);
		
		userNotificationRepository.save(userNotification); 
	}

	public List<Notification> getNotification(String userId) {
		
		UserNotification userNotification = userNotificationRepository.findById(userId)
				.orElseThrow(RuntimeException::new);
		return userNotification.getNotifications();
	}

	public List<UserNotification> getAllUsersWithNotifications() {

		return userNotificationRepository.findAll();
	}
}
