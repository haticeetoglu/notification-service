package com.buggerspace.notificationservice.service;

import java.time.LocalDateTime;
import java.util.List;

import com.buggerspace.notificationservice.domain.ProductPriceChangeNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.buggerspace.notificationservice.domain.Notification;
import com.buggerspace.notificationservice.domain.UserNotification;

@Service
public class KafkaService {
	
	@Autowired
	UserNotificationService userNotificationService;
	//@Autowired
	//MailService mailService;

	@KafkaListener(
            topics = "${spring.kafka.topic.product-price-change-notification}",
            groupId = "product-price-change-group",
            clientIdPrefix = "product-price-change-group",
            containerFactory = "productPriceChangeQueueKafkaListenerContainerFactory"
    )
	public void getNotification(ProductPriceChangeNotification productPriceChangeNotification) {
		String productId=Long.toString(productPriceChangeNotification.getProductId());
		
		for(int i=0;i<productPriceChangeNotification.getUserIds().size();i++) {
			String userId=Integer.toString(productPriceChangeNotification.getUserIds().get(i));
			Notification notification = new Notification(productId,productPriceChangeNotification.getProductName(),LocalDateTime.now());
			
			UserNotification userNotification;// userNotificationService.findByUserId(userId);.orElse(new UserNotification(userId,"example@hotmail.com",true,true));
			try {
				userNotification= userNotificationService.findByUserId(userId);
			}
			catch(Exception e){
				userNotification = new UserNotification(userId,"example"+userId+"@hotmail.com",true,true);
			}
			userNotification.setNotifications(notification);
			userNotificationService.create(userNotification);
			
			if(userNotification.isMobileNotificationsIsActive()){
				//mailService.sendFromGMail(userNotification.getEmail(), "Fiyat Düştü", String.format("Fiyat %s'den %s'ye düştü.",productPriceChangeNotification.getOldPrice(), productPriceChangeNotification.getNewPrice()));
			}
				//System.out.println("Push Notification sent to UserId : " + userNotification.getUserId()+" for ProductId : " + productId);
			
			if(userNotification.isWebNotificationsIsActive()){
				//mailService.sendFromGMail(userNotification.getEmail(), "Fiyat Düştü", String.format("Fiyat %s'den %s'ye düştü.",productPriceChangeNotification.getOldPrice(), productPriceChangeNotification.getNewPrice()));
			}
				//System.out.println("Email sent to UserId : " + userNotification.getUserId()+" for ProductId : " + productId + " to : " + userNotification.getEmail());
		}
		
	}
}
