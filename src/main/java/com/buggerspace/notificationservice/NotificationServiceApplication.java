package com.buggerspace.notificationservice;

import java.time.LocalDateTime;

import com.buggerspace.notificationservice.service.UserNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.buggerspace.notificationservice.domain.Notification;
import com.buggerspace.notificationservice.domain.UserNotification;
import com.buggerspace.notificationservice.repository.UserNotificationRepository;

@SpringBootApplication
public class NotificationServiceApplication implements CommandLineRunner {

	public static void main(String[] args) {
		
		SpringApplication.run(NotificationServiceApplication.class, args);
	
	}
	
	@Autowired
	private UserNotificationRepository repo;

	@Autowired
	private UserNotificationService userNotificationService;

	@Override
	public void run(String... args) throws Exception { // 1. Delete Previous records
		//repo.deleteAll();
		UserNotification userNotification1 = new UserNotification("19", "haticeetoglu03@gmail.com", false, false );
		userNotification1.setNotifications(new Notification("26", "Masa", LocalDateTime.now()));
		userNotification1.setNotifications(new Notification("27", "Sandalye", LocalDateTime.now()));
		userNotification1.setNotifications(new Notification("28", "Koltuk", LocalDateTime.now()));
		userNotificationService.create(userNotification1);
/*
		UserNotification userNotification2 = new UserNotification("2", "test2@hotmail.com", false, true );

		userNotification2.setNotifications(new Notification("14", 77.99, 45.99, LocalDateTime.now()));
		userNotification2.setNotifications(new Notification("15", 100.00, 89.99, LocalDateTime.now()));
		userNotification2.setNotifications(new Notification("16", 34.99, 8.99, LocalDateTime.now()));
		userNotificationService.create(userNotification2);

		UserNotification userNotification3 = new UserNotification("3", "test3@hotmail.com", true, false );

		userNotification3.setNotifications(new Notification("17", 77.99, 45.99, LocalDateTime.now()));
		userNotification3.setNotifications(new Notification("18", 100.00, 89.99, LocalDateTime.now()));
		userNotification3.setNotifications(new Notification("19", 34.99, 8.99, LocalDateTime.now()));
		userNotificationService.create(userNotification3);*/

	}

}
