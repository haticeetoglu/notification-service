package com.buggerspace.notificationservice.domain;

import org.springframework.data.couchbase.core.mapping.Field;

import java.time.LocalDateTime;

public class Notification {

	@Field
	private String productId;
	
	@Field
	private String productName;
	
	@Field
	private LocalDateTime notificationDate;
	
	public Notification(String productId, String productName, LocalDateTime notificationDate) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.notificationDate = notificationDate;
	}

	public String getProductId() {
		
		return productId;
	}

	public void setProductId(String productId) {
		
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public LocalDateTime getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(LocalDateTime notificationDate) {
		this.notificationDate = notificationDate;
	}

	@Override
	public String toString() {
		return "Notification [productId=" + productId + ", productName=" + productName
				+ ", notificationDate=" + notificationDate + "]";
	}
	
	
}
