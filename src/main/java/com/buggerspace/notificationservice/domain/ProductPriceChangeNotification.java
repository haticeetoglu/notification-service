package com.buggerspace.notificationservice.domain;

import java.util.List;

public class ProductPriceChangeNotification {
	
	private long productId;
	private String productName;
	private List<Integer> userIds;
	
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public List<Integer> getUserIds() {
		return userIds;
	}
	public void setUserId(List<Integer> userIds) {
		this.userIds = userIds;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	

	
}
