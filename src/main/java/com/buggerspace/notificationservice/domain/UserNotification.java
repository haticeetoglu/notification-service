package com.buggerspace.notificationservice.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import com.buggerspace.notificationservice.service.UserNotificationService;
import org.springframework.data.couchbase.core.mapping.Field;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.GenerationStrategy;

@Document
public class UserNotification {

	UserNotificationService userNotificationService;

	@Id
	@Field
	private String userId;

	@Field
	private String email;

	@Field
	private boolean webNotificationsIsActive;

	@Field
	private boolean mobileNotificationsIsActive;

	@Field
	private List<Notification> notifications;

	public UserNotification() {
		
		super();
		this.notifications = new ArrayList<Notification>();
	}

	public UserNotification(String userId, String email, boolean webNotificationsIsActive, boolean mobileNotificationsIsActive) {
		
		super();
		this.userId = userId;
		this.email = email;
		this.webNotificationsIsActive = webNotificationsIsActive;
		this.mobileNotificationsIsActive = mobileNotificationsIsActive;
		this.notifications = new ArrayList<Notification>();
	}

	public String getEmail() {
		
		return email;
	}

	public void setEmail(String email) {
		
		this.email = email;
	}

	public String getUserId() {
		
		return userId;
	}

	public void setUserId(String userId) {
		
		this.userId = userId;
	}

	public boolean isWebNotificationsIsActive() {
		
		return webNotificationsIsActive;
	}

	public void setWebNotificationsIsActive(boolean webNotificationsIsActive) {
		
		this.webNotificationsIsActive = webNotificationsIsActive;
	}

	public boolean isMobileNotificationsIsActive() {
		
		return mobileNotificationsIsActive;
	}

	public void setMobileNotificationsIsActive(boolean mobileNotificationsIsActive) {
		
		this.mobileNotificationsIsActive = mobileNotificationsIsActive;
	}

	public List<Notification> getNotifications() {
		
		return notifications;
	}

	public void setNotifications(Notification notification) {
		
		this.notifications.add(notification);
	}	
	
	@Override
	public String toString() {
		
		return "UserNotification [userId=" + userId + ", webNotificationsIsActive=" + webNotificationsIsActive
				+ ", mobileNotificationsIsActive=" + mobileNotificationsIsActive + "]";
	}

}
